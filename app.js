function AppCtrl($scope) {
    $scope.new = 0;
    $scope.types = [];
    $scope.solution = [];
    $scope.resume = [];
    $scope.add = function()
    {
        if (isNaN($scope.new.capacite) || $scope.new <= 0 || $scope.types.indexOf($scope.new) >=0)
            alert('La variable doit être un nombre suprérieur à 0');
        else
        {
	    $scope.new.nombre = 10000000; //almost infinity
            $scope.types.push($scope.new);
            $scope.new = 0;
        }
    };
    var s = '';
    $scope.execute = function()
    {
        if ($scope.types.length == 0 || $scope.x <=0)
            return alert('Insérez les donneés d\'abord');
        //init array
        $scope.solution = ['Pour ' + $scope.x + ' clients'];
        //sort the types to start with lowest capacity
        $scope.types.sort(function(a, b){return a.capacite-b.capacite});
        recursive($scope.x, $scope.types.length-1);
        var temp = $scope.resume.join('%').split('fin');
        temp = temp.filter(function(t)
        {
           return t[0] != '-';
        });

        $scope.final = temp.join('%').replace(/%/g, '\n\r').replace(/\+/g, 'ou');
        $scope.final = $scope.final.split('\n\r').filter(function(t){
           return t.indexOf('*0') < 0;
        });
         if ($scope.final[$scope.final.length -1] == 'ou')
            $scope.final.pop();
        $scope.final = $scope.final.join('\n\r');

    };

    function recursive(nombre, type)
    {
        var restant = nombre;
        if(nombre > 0)
        {
            if (type > 0)
            {
                var nbmax = maxChambre(nombre, type);
                for (var i=nbmax; i>=0; i--)
                {
                    restant = nombre - $scope.types[type].capacite * i;
                    $scope.resume.push(whiteSpace($scope.types.length - type -1) +'chambre' + $scope.types[type].capacite + '*'+ i);
                    $scope.solution.push(whiteSpace($scope.types.length - type -1) + ' Pour ' + i + ' chambres de capacité ' + $scope.types[type].capacite + ' Il reste ' + restant + ' clients');
                    recursive(restant, type - 1);
                }
            }
            else if (nombre > $scope.types[type].nombre * $scope.types[type].capacite)
            {
                $scope.resume.push('fin-');
                $scope.solution.push('* Fausse solution. Il reste ' + nombre + ' client(s) et il y a ' + $scope.types[type].nombre + ' chambres de capacite ' + $scope.types[type].capacite);
            }
            else
            {
                if (nombre % $scope.types[type].capacite > 0)
                {
                    $scope.resume.push('fin-');
                    $scope.solution.push(whiteSpace($scope.types.length) + ' * Fin pas de solution');
                }
                else
                {
                    $scope.resume.push(whiteSpace($scope.types.length) +'Chambre' + $scope.types[type].capacite + '*' +  nombre / $scope.types[type].capacite);
                    $scope.resume.push('fin+');
                    $scope.solution.push(whiteSpace($scope.types.length) + ' *' + nombre / $scope.types[type].capacite + ' Chambres de capacité ' + $scope.types[type].capacite + ' Fin de solution.');
                }
            }
        }
        else
        {
            $scope.resume.push('fin+');
            $scope.solution.push(whiteSpace($scope.types.length) +'* Fin de solution.');
        }
    }

    function maxChambre(nbClient, numChambre)
    {
        return Math.min(Math.floor(nbClient/$scope.types[numChambre].capacite), $scope.types[numChambre].nombre);
    }

    function whiteSpace(type)
    {
        var chaine = '';
        for (var i=0; i<type;i++)
            chaine += '  ';
        return chaine;
    }

    function parse()
    {
        var copy = new Array().concat($scope.solutions);
        var element = [];
    }
}
